# Comments API

## [**Demo**](https://laravelnestedcomments.herokuapp.com/api/comments)

## **Show All Comments**

Get JSON array of all comments in a treelike structure - comments which are replies are placed in a `children` property of a parent comment.

**URL** : `/api/comments/`

**Method** : `GET`

### Success Response

**Code** : `200 OK`

**Response body example**

```json
[
    {
        "id": 1,
        "parent_id": null,
        "author": "Joe Doe",
        "title": "sample title",
        "content": "Lorem ipsum dolor sit amet",
        "votes": 17,
        "created_at": "2020-09-11T10:14:38.000000Z",
        "updated_at": "2020-09-11T10:14:38.000000Z",
        "children": [
            {
                "id": 2,
                "parent_id": 1,
                "author": "Joe Doe",
                "title": "sample title",
                "content": "Lorem ipsum dolor sit amet",
                "votes": -10,
                "created_at": "2020-09-11T10:14:38.000000Z",
                "updated_at": "2020-09-11T10:14:38.000000Z",
                "children": []
            }
        ]
    }
]
```

### Notes

Comments at the same nesting level are ordered by votes (descending) and creation time (starting from newest).

## **Show Single Comment**

Get JSON object representing single comment (without children property).

**URL** : `/api/comments/{comment_id}`

**Method** : `GET`

### Success Response

**Code** : `200 OK`

**Response body example**

```json
{
    "id": 1,
    "parent_id": null,
    "author": "Joe Doe",
    "title": "sample title",
    "content": "Lorem ipsum dolor sit amet",
    "votes": 17,
    "created_at": "2020-09-11T10:14:38.000000Z",
    "updated_at": "2020-09-11T10:14:38.000000Z"
}
```

### Notes

-   Providing `comment_id` which does not identify any comment entity will cause response failure with status code `400 BAD REQUEST`.
    Same applies to any nonnumerical value.

```json
{
    "error": "Comment with a given id not found."
}
```

## **Create Comment**

Create new comment entity.

**URL** : `/api/comments`

**Method** : `POST`

### Success Response

**Code** : `201 OK`

### Data constraints

All fields except `parent_id` must be provided.

```json
{
    "parent_id": "[null | number]",
    "author": "[64 chars max]",
    "title": "[255 chars max]",
    "content": "[65 535 chars max]"
}
```

### Notes

-   In case of `parent_id` absence it's being set to `null` which causes comment to be at top level of a structure.

-   In case of providing arguments which does not meet validation conditions failure response is being sent with status code `400 BAD REQUEST`.

**Response body example**

```json
{
    "error": {
        "title": ["The title field is required."]
    }
}
```

```json
{
    "error": "Parent comment with a given id not found."
}
```

## **Vote**

Increment or decrement comment's votes.

**URL** : `/api/comments/{comment_id}/votes`

**Method** : `PUT`

### Success Response

**Code** : `200 OK`

### Data constraints

Increment field is required.

```json
{
    "increment": "[boolean]"
}
```

### Notes

-   In case of providing `comment_id` which does not identify any comment entity will cause response failure with status code `400 BAD REQUEST`. Same applies to any nonnumerical value.

-   To decrement votes value increment field has to be false.
