<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CommentsController;

Route::apiResource('comments', CommentsController::class)->only('index', 'store', 'show');
Route::put('comments/{comment}/votes', [CommentsController::class, 'vote']);