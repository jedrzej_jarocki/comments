<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            return response()->json([
                'error' => str_replace('App\\Models\\', '', $exception->getModel()).' with a given id not found.'], 404);
        }
        
        if ($exception instanceof NotFoundHttpException) {
            return response()->json(['error' => 'Not found.'], 404);
        }

        /*  Foreign key constraint violation handling.
            Executes if parent_id given in comment creation request body
            isn't null and doesn't match any comment id.
        */
        if ($exception->errorInfo[1] === 1452) {
            return response()->json(['error' => 'Parent comment with a given id not found.'], 400);
        }

        return parent::render($request, $exception);
    }
}
