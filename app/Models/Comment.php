<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['parent_id', 'author', 'title', 'content'];

    private function immediateChildren()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function children()
    {
        return $this->immediateChildren()
            ->orderBy('votes','desc')
            ->orderBy('created_at','desc')
            ->with('children');
    }
}
