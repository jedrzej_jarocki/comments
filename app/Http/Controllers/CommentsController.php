<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Validator;
use DB;

class CommentsController extends Controller
{

    protected function hasValidationErrors($request) {
        $rules = [
            'author' => 'required|max:63',
            'title' => 'required|max:255',
            'content' => 'required|max:65535'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return $validator->errors();
        }

        // Check if provided parent_id is not equal to next auto incremented primary key.
        $statement = DB::select("SHOW TABLE STATUS LIKE 'comments'");
        $nextId = $statement[0]->Auto_increment;

        if($nextId === $request->parent_id) {
            return 'Parent comment with a given id not found.';
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['a' => 'a']);
        // $comments=Comment::where(['parent_id'=> null])
        //     ->orderBy('votes','desc')
        //     ->orderBy('created_at','desc')
        //     ->with('children')
        //     ->get();
        // return response()->json($comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $error = $this->hasValidationErrors($request);
        if($error) {
            return response()->json(['error' => $error], 400);
        }

        Comment::create($request->all());
        return response('', 201)->header('Content-Type', 'application/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return response()->json($comment);
    }

    public function vote(Request $request, Comment $comment)
    {
        $increment = $request->increment;

        if (!is_bool($increment)) {
            return response()->json(['error' => 'The increment filed must be a boolean value.'], 400);
        }

        $comment->votes += $increment === true ? 1 : -1;
        $comment->save();
        return response('')->header('Content-Type', 'application/json');;
    }
}
